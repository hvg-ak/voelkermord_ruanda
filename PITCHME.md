﻿<style>
    .reveal .slides section .hcg .fragment.fade-down {
        opacity: 1;
        visibility: visible;
    }
    .reveal .slides section .hcg .fragment.fade-down.visible,
    .reveal .slides section .hcg .fragment.visible:not(.current-fragment) {
        color: grey;
    }
    .reveal .slides section .hcg .fragment.fade-down,
    .reveal .slides section .hcg .fragment.current-fragment {
        color: #1b91ff;
    }
    .slide-number{
      display: none !important;
    }
    .controls{
      display: none !important;
    }
</style>
## Menschenrechtsverletzung
### am Völkermord von Ruanda
##### <span style="font-family:Helvetica Neue; font-weight:bold">Von <span style="color:#e49436">Alexander</span></span>

---
<iframe width="560" height="315" src="https://www.youtube.com/embed/CJ7uiSnAjq0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
---

## Quellen

Literatur:
<ul style="font-size: 13px !important;">
<li>"ZEITEN UND MENSCHEN", Verlag Schöningh, Druck 2017, 978-3-14-024946-1</li>
<li>https://www.liportal.de/ruanda/geschichte-staat/, Aufgerufen am 24.06.18</li>
<li>https://brockhaus.de/ecs/enzy/article/ruanda, Aufgerufen am 23.06.18</li>
</ul><br>
Medien:
<br>
<ul style="font-size: 13px !important;">
<li>Globalisierung (Völkermord in Ruanda), https://www.youtube.com/watch?v=NUKeNYFS94E, Aufgerufen am: 24.06.18</li>
<li>Verrat an der Menschlichkeit - Ruanda - Teil 1, https://www.youtube.com/watch?v=Vzdy0urRg9s, Aufgerufen am 24.06.18</li>
</ul>